////  SwifyUI2_TrekerApp.swift
//  SwifyUI2_Treker
//
//  Created on 25/01/2021.
//  
//

import SwiftUI

@main
struct SwifyUI2_TrekerApp: App {
    
    @StateObject var locations = Locations()
    
    var body: some Scene {
        WindowGroup {
            
            TabView {
                
                NavigationView {
                    ContentView(location: locations.secendary )
                } //: NavigationView
                .tabItem {
                    Image(systemName: "airplane.circle.fill")
                    Text("Discover")
                }
                
                NavigationView {
                    WorldView()
                } //: NavigationView
                .tabItem {
                    Image(systemName: "airplane.circle.fill")
                    Text("Locations")
                }
                
                NavigationView {
                    TipView()
                } //: NavigationView
                .tabItem {
                    Image(systemName: "airplane.circle.fill")
                    Text("Tips")
                }
                
            } //: TabView
            .environmentObject(locations)
        }
    }
}
